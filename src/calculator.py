class Calculator(object):

    def __init__(self, sum, sub):
        self.sum = sum
        self.sub = sub

    def addition(self, num_1, num_2, op):
        if op:
            return self.sum.sum(num_1, num_2)
        return None

    def subtraction(self, num_1, num_2, op):
        if op:
            return self.sub.sub(num_1, num_2)
        return None