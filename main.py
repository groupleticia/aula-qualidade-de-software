from src.calculator import Calculator
from src.operations.sum import SumOperations
from src.operations.sub import SubOperations

calculator = Calculator(SumOperations(), SubOperations())

operation_1 = calculator.addition(2, 5, True)
operation_2 = calculator.subtraction(5, 3, True)

print(f'Operação 1 = {operation_1}')
print(f'Operação 2 = {operation_2}')