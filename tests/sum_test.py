from src.operations.sum import SumOperations

from faker import Faker

fake = Faker()

def test_sum():
    sumOperation = SumOperations()

    num_1 = fake.random_number()
    num_2 = fake.random_number()

    expected_sum = num_1 + num_2

    result = sumOperation.sum(num_1,num_2)

    assert result == num_1 + num_2