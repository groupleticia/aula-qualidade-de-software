from src.operations.sub import SubOperations

from faker import Faker

fake = Faker()

def test_sub():
    subOperation = SubOperations()

    num_1 = fake.random_number()
    num_2 = fake.random_number()

    expected_sum = num_1 - num_2

    result = subOperation.sub(num_1,num_2)

    assert result == num_1 - num_2